from django.contrib import admin
from schedule.models import Occurrence
from apps.accounts.models import Professional

class AdImageInline(admin.TabularInline):
    # model = Professional
    model = Professional.ocurrences.through
    extra = 1


class OccurrenceAdmin(admin.ModelAdmin):
    inlines = [AdImageInline]
    # pass

admin.site.register(Occurrence, OccurrenceAdmin)

