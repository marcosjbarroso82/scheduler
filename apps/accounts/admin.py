from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin
 
from .models import CustomUser, Patient, Professional
from .forms import CustomUserCreationForm, PatientCreationForm, ProfessionalCreationForm
 
class CustomUserAdmin(UserAdmin):
    form = CustomUserCreationForm


 
# admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(CustomUser, UserAdmin)
admin.site.register(Patient, UserAdmin)
admin.site.register(Professional, UserAdmin)
