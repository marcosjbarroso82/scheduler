from django.db import models
from django.contrib.auth.models import AbstractUser
from schedule.models import Occurrence
 
class CustomUser(AbstractUser):
    pass
 
class Patient(CustomUser):

    class Meta:
        verbose_name = 'Patient'


class Professional(CustomUser):
    ocurrences = models.ManyToManyField(Occurrence)


    class Meta:
        verbose_name = 'Professional'