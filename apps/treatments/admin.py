from django.contrib import admin
from .models import Treatment
from django.forms import ModelForm
from schedule.models import Event


# class CustomSitePhoneInline(admin.TabularInline):
#     model = Event
#     form = ModelForm
#     extra = 1


class TreatmentAdmin(admin.ModelAdmin):
    list_display = ('name',)
    # inlines = [CustomSitePhoneInline, ]


admin.site.register(Treatment, TreatmentAdmin)
